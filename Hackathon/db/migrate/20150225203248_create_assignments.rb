class CreateAssignments < ActiveRecord::Migration
  def change
    create_table :assignments do |t|
      t.integer :assignment_id
      t.string :employee_id
      t.string :integer
      t.integer :job_id
      t.integer :task_id

      t.timestamps null: false
    end
  end
end
