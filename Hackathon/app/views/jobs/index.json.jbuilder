json.array!(@jobs) do |job|
  json.extract! job, :id, :job_id, :job_title, :location, :due_date
  json.url job_url(job, format: :json)
end
